#pragma once

#include <iostream>
#include <pthread.h>
#include <vector>
#include <semaphore.h>

using namespace std;

template <class T>
class RingQueue
{
private:
    void P(sem_t &s)
    {
        sem_wait(&s);
    }

    void V(sem_t &s)
    {
        sem_post(&s);
    }

public:
    RingQueue(const size_t max_cap) // 构造环形队列
        : _ringqueue(max_cap), _max_cap(max_cap), _c_step(0), _p_step(0)
    {
        sem_init(&_data_sem, 0, 0); // 初始化信号量
        sem_init(&_emp_sem, 0, _max_cap);

        pthread_mutex_init(&_p_mutex, nullptr);
        pthread_mutex_init(&_c_mutex, nullptr);
    }

    void Push(const T &in)
    {                // 生产者
        P(_emp_sem); // 申请空间
        pthread_mutex_lock(&_p_mutex);
        _ringqueue[_p_step] = in;
        _p_step++;
        _p_step = _p_step % _max_cap;
        pthread_mutex_unlock(&_p_mutex);
        V(_data_sem); // 释放数据
    }
    void Front(T *out) // 消费者
    {
        P(_data_sem); // 申请数据
        pthread_mutex_lock(&_c_mutex);
        *out = _ringqueue[_c_step];
        _c_step++;
        _c_step = _c_step % _max_cap;
        pthread_mutex_unlock(&_c_mutex);
        V(_emp_sem); // 释放空间
    }

    ~RingQueue() // 析构销毁资源
    {
        sem_destroy(&_data_sem);
        sem_destroy(&_emp_sem);

        pthread_mutex_destroy(&_p_mutex);
        pthread_mutex_destroy(&_c_mutex);
    }

private:
    vector<T> _ringqueue;
    int _max_cap;

    int _c_step; // 消费者指针的位置
    int _p_step; // 生产者指针的位置

    sem_t _data_sem; // 表示数据个数
    sem_t _emp_sem;  // 表示空闲空间个数

    pthread_mutex_t _p_mutex;
    pthread_mutex_t _c_mutex;
};

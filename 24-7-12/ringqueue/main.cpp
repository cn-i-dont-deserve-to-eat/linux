

#include "RingQueue.hpp"
#include <unistd.h>
using namespace std;

void *Consumer(void *arg)
{
    RingQueue<int> *rq = static_cast<RingQueue<int> *>(arg);
    while (true)
    {
        int data = 0;
        rq->Front(&data);
        cout << "生产者获得数据->" << data << endl;
        sleep(1);
    }
}

void *Produce(void *arg)
{
    RingQueue<int> *rq = static_cast<RingQueue<int> *>(arg);
    while (true)
    {
        int data = rand() % 100;
        rq->Push(data);
        cout << "生产者生产了数据->" << data << endl;
        sleep(1);
    }
}

int main()
{
    pthread_t t1, t2, t3, t4;
    srand(time(nullptr));
    RingQueue<int> *rq = new RingQueue<int>(10);

    pthread_create(&t1, nullptr, Consumer, rq);
    pthread_create(&t2, nullptr, Produce, rq);
    pthread_create(&t3, nullptr, Consumer, rq);
    pthread_create(&t4, nullptr, Produce, rq);

    pthread_join(t1, nullptr);
    pthread_join(t2, nullptr);
    pthread_join(t3, nullptr);
    pthread_join(t4, nullptr);
    return 0;
}
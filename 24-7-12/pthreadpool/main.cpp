#include "ThreadPool.hpp"
#include "Task.hpp"
#include <unistd.h>

int main()
{
    ThreadPool<Task> *tp = new ThreadPool<Task>();
    tp->Init();
    tp->Start();
    int cnt = 10;
    srand(time(nullptr));
    while (cnt)
    {
        // 主线程安排任务给线程池
        sleep(1);
        int x = rand() % 100;
        int y = rand() % 100;
        Task t(x, y);
        tp->Push(t);
        sleep(1);
        cout << "cnt: " << cnt-- << endl;
    }
    tp->Stop();
    cout << "threadpoll is end" << endl;
    sleep(10);

    return 0;
}
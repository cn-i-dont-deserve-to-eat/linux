#include "TcpServer.hpp"
#include <iostream>
#include <memory>
#include "Command.hpp"
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " local-port" << std::endl;
        exit(0);
    }

    uint16_t port = std::stoi(argv[1]);
    Command cmdservice;
    std::unique_ptr<TcpServer> tcvr = make_unique<TcpServer>(std::bind(&Command::HandlerCommand, &cmdservice, std::placeholders::_1, std::placeholders::_2), port);

    tcvr->InitServer();
    tcvr->Loop();

    return 0;
}
#pragma once
#include <iostream>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fstream>
#include <pthread.h>
#include <functional>
#include "InetAddr.hpp"
#include "Log.hpp"

using namespace log_ns;

const static int gport = 8888;
const static int gsockfd = -1;
const static int gblcklog = 8;

using command_service_t = std::function<void(int sockfd, InetAddr addr)>;

class TcpServer
{
private:
public:
    TcpServer(command_service_t service, uint16_t port = gport)
        : _port(port), _listensockfd(gsockfd), _isrunning(false), _service(service)
    {
    }
    void InitServer()
    {
        // 1.创建socket,选择TCP协议
        _listensockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (_listensockfd < 0)
        {
            LOG(FATAL, "socket create error\n");
            exit(1);
        }
        LOG(FATAL, "listen socket create success,sockfd is %d\n", _listensockfd);

        // 创建strcut sockaddr_in
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = INADDR_ANY;

        // 2. bind
        int res = bind(_listensockfd, (struct sockaddr *)&local, sizeof(local));
        if (res < 0)
        {
            LOG(FATAL, "server listen bind failed\n");
            exit(1);
        }
        LOG(FATAL, "server listen bind success\n");

        // 3.设置监听状态
        if (listen(_listensockfd, gblcklog) < 0)
        {
            LOG(FATAL, "listen error\n");
            exit(1);
        }
        LOG(INFO, "listen suceess\n");
    }

    class ThreadDate
    {
    public:
        int _sockfd;
        TcpServer *_self;
        InetAddr _addr;

        ThreadDate(int sockfd, TcpServer *self, const InetAddr &addr)
            : _sockfd(sockfd), _self(self), _addr(addr)
        {
        }
    };

    void Loop()
    {
        _isrunning = true;
        while (_isrunning)
        {
            struct sockaddr_in client;
            socklen_t len = sizeof(client);
            // 4.获取新连接
            int sockfd = accept(_listensockfd, (struct sockaddr *)&client, &len);
            if (sockfd < 0)
            {
                LOG(WARNING, "accept error\n");
                continue;
            }
            InetAddr addr(client);

            LOG(INFO, "get a new link,client info:%s,sockfd is %d\n", addr.AddrStr().c_str(), sockfd);
            // 处理业务
            // v0--简单读取消息
            // Service(sockfd, addr);

            // v2--线程处理
            pthread_t tid;
            ThreadDate *td = new ThreadDate(sockfd, this, addr);
            LOG(DEBUG, "create pthread\n");
            pthread_create(&tid, nullptr, Execute, td);
        }
        _isrunning = false;
    }

    static void *Execute(void *args)
    {
        pthread_detach(pthread_self());
        ThreadDate *td = static_cast<ThreadDate *>(args);
        td->_self->_service(td->_sockfd, td->_addr);
        delete td;
        return nullptr;
    }

    // void Service(int sockfd, InetAddr addr)
    // {
    //     while (true)
    //     {
    //         // 接收消息
    //         char inbuff[1024];
    //         ssize_t n = read(sockfd, inbuff, sizeof(inbuff) - 1);
    //         if (n > 0)
    //         {
    //             inbuff[n] = 0;
    //             LOG(INFO, "get message from client ,message is %s\n", inbuff);
    //             // 回响
    //             string echo_message = "[server say]# ";
    //             echo_message += inbuff;
    //             write(sockfd, echo_message.c_str(), echo_message.size());
    //         }
    //         else if (n == 0)
    //         {
    //             LOG(INFO, "client %s quit\n", addr.AddrStr().c_str());
    //             break;
    //         }
    //         else
    //         {
    //             LOG(ERROR, "read error: %s\n", addr.AddrStr().c_str());
    //             break;
    //         }
    //     }
    //     close(sockfd);
    // }

private:
    uint16_t _port;
    int _listensockfd;
    bool _isrunning;
    command_service_t _service;
};

#include "UdpServer.hpp"
#include <iostream>
#include <memory>
using namespace std;

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cerr << "Usage:" << argv[0] << " local-port" << endl;
        exit(0);
    }

    uint16_t port = stoi(argv[1]);
    EnableScreen();
    unique_ptr<UdpServer> usvr = make_unique<UdpServer>(port);
    usvr->InitServer();
    usvr->Start();
    return 0;
}
#pragma once
#include <functional>
#include "Socket.hpp"
#include "Log.hpp"
#include "InetAddr.hpp"

using namespace socket_ns;
static const int gport = 8888;

using service_io_t = function<void(SockSPtr, InetAddr &)>;

class TcpServer
{
public:
    TcpServer(service_io_t service, int port = gport)
        : _service(service), _port(port), _listensock(std::make_shared<TcpSocket>()), _isrunning(false)
    {
        _listensock->BuildListenSocket(_port);
    }
    ~TcpServer() {}

    class ThreadData
    {
    public:
        SockSPtr _sockfd;
        TcpServer *_self;
        InetAddr _addr;

        ThreadData(SockSPtr sockfd, TcpServer *self, const InetAddr &addr)
            : _sockfd(sockfd), _self(self), _addr(addr)
        {
        }
    };

    void Loop()
    {
        _isrunning = true;
        while (_isrunning)
        {
            InetAddr client;
            SockSPtr newsock = _listensock->Accepter(&client);
            if (newsock == nullptr)
            {
                continue;
            }
            LOG(INFO, "get a link,client info : %s,sockfd is :%d\n", client.AddrStr().c_str(), newsock->Sockfd());

            pthread_t tid;
            ThreadData *td = new ThreadData(newsock, this, client);
            pthread_create(&tid, nullptr, Execute, td);
        }
        _isrunning = false;
    }

    static void *Execute(void *arg)
    {
        pthread_detach(pthread_self());
        ThreadData *td = static_cast<ThreadData *>(arg);
        td->_self->_service(td->_sockfd, td->_addr);
        td->_sockfd->Sockfd();
        delete td;
        return nullptr;
    }

private:
    uint16_t _port;
    SockSPtr _listensock;
    bool _isrunning;
    service_io_t _service;
};

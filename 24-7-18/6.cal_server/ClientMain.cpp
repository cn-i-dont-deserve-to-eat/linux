#include <iostream>
#include <ctime>
#include <unistd.h>
#include "Socket.hpp"
#include "Protocol.hpp"

using namespace socket_ns;

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cerr << "Usage: " << argv[0] << " server-ip server-port" << std::endl;
        exit(0);
    }

    std::string serverip = argv[1];
    uint16_t serverport = std::stoi(argv[2]);

    SockSPtr sock = std::make_shared<TcpSocket>();
    if (!sock->BuildClientSocket(serverip, serverport))
    {
        std::cerr << "connect error" << std::endl;
        exit(1);
    }

    srand(time(nullptr));
    const std::string opers = "+-*/";
    int cnt = 5;
    std::string packagestreamqueue;
    while (true)
    {
        // 构建数据
        int x = rand() % 100;
        usleep(x * 1000);
        int y = rand() % 10;
        usleep(y * 1000);
        char oper = opers[y % opers.size()];

        // 构建请求
        auto res = Factory::BuildRequestDefault();
        res->SetValue(x, y, oper);

        // 1.序列化
        std::string reqstr;
        res->Serialize(&reqstr);

        // 2.添加报文
        reqstr = Encode(reqstr);
        std::cout << "####################################" << std::endl;

        std::cout << "request string: \n"
                  << reqstr << std::endl;

        // 3. 发送数据
        sock->Send(reqstr);

        // 4.接收应答
        while (true)
        {
            ssize_t n = sock->Recv(&packagestreamqueue);
            if (n <= 0)
            {
                break;
            }

            // 5.报文解析
            std::string package = Decode(packagestreamqueue);
            if (package.empty())
            { // 读取到的报文不完整
                continue;
            }

            // 6.反序列化
            auto resp = Factory::BuildResponseDefault();
            resp->Deserialize(package);
            // 7.输出结果
            resp->PrintResult();
            break;
        }
        sleep(1);
    }
    sock->Close();

    return 0;
}
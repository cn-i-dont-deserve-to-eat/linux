#include "UdpServer.hpp"
#include <iostream>
#include <string>
#include <memory>
#include "Route.hpp"
using namespace std;

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cerr << "Usage:" << argv[0] << " local-port" << endl;
        exit(0);
    }

    uint16_t port = stoi(argv[1]);
    EnableScreen();
    Route messageRoute;
    server_t message_route = bind(&Route::Forward,
                                  &messageRoute, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    std::unique_ptr<UdpServer> usvr = std::make_unique<UdpServer>(message_route, port); // C++14的标准
    usvr->InitServer();
    usvr->Start();
    return 0;
}
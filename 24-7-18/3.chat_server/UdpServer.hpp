#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string>
#include <cstring>
#include "LockGuard.hpp"
#include "Log.hpp"
#include "nocopy.hpp"
#include "InetAddr.hpp"
#include <functional>
using namespace log_ns;
using namespace std;
static const int gsockfd = -1;
static const uint16_t glocalport = 8888;
using server_t = function<void(int, const string &, InetAddr &)>;
enum
{
    SOCKET_ERROR = 1,
    BIND_ERROR
};

class UdpServer : public nocopy
{
private:
public:
    UdpServer(server_t func, uint16_t localport = glocalport)
        : _localport(localport), _isrunning(false), _sockfd(gsockfd), _fun(func)
    {
    }

    void InitServer()
    {
        // 创建socket
        _sockfd = socket(AF_INET, SOCK_DGRAM, 0); // IPv4协议和UDP协议的套接字
        if (_sockfd < 0)
        {
            LOG(FATAL, "socket create error!\n");
            exit(1);
        }
        LOG(DEBUG, "socket create success,sockfd is %d\n", _sockfd);
        // 绑定端口号和IP
        // 绑定之前创建一个sockaddr_in对象,存储本地地址信息
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_addr.s_addr = INADDR_ANY; // 服务器端，进行任意IP地址绑定
        local.sin_port = htons(_localport);

        int n = bind(_sockfd, (struct sockaddr *)&local, sizeof(local));
        if (n < 0)
        {
            LOG(FATAL, "sockfd bind error!\n");
            exit(1);
        }
        LOG(DEBUG, "sockfd bind success!\n");
    }

    void Start()
    {
        _isrunning = true;
        char inbuff[1024];
        while (_isrunning)
        {
            // 获取数据
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);

            ssize_t n = recvfrom(_sockfd, inbuff, sizeof(inbuff) - 1, 0, (struct sockaddr *)&peer, &len);
            if (n < 0)
            {
                LOG(FATAL, "recvfrom error!\n");
            }
            else
            {
                LOG(DEBUG, "recvfrom success!\n");
                InetAddr addr(peer);
                inbuff[n] = '\0';
                std::string message = inbuff;
                _fun(_sockfd, message, addr);
            }
        }
        _isrunning = false;
    }

    ~UdpServer()
    {
        if (_sockfd >= 0)
            close(_sockfd);
    }

private:
    int _sockfd;
    uint16_t _localport;
    bool _isrunning;
    server_t _fun;
};
#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
using namespace std;
// 创建一个类用于将网络地址
class InetAddr
{
private:
    void ToHest(const sockaddr_in &addr)
    {
        _port = ntohs(addr.sin_port);
        _ip = inet_ntoa(addr.sin_addr);
    }

public:
    InetAddr(const sockaddr_in &addr)
        : _addr(addr)
    {
        ToHest(addr);
    }

    string Ip()
    {
        return _ip;
    }

    uint16_t Port()
    {
        return _port;
    }

    bool operator==(const InetAddr &addr)
    {
        return (this->_ip == addr._ip && this->_port == addr._port);
    }
    string AddrStr()
    {
        return _ip + ":" + to_string(_port);
    }

    struct sockaddr_in Addr()
    {
        return _addr;
    }

private:
    string _ip;
    uint16_t _port;
    struct sockaddr_in _addr;
};
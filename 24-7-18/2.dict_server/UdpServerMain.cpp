#include "UdpServer.hpp"
#include <iostream>
#include <memory>
#include "Dict.hpp"
using namespace std;

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cerr << "Usage:" << argv[0] << " local-port" << endl;
        exit(0);
    }

    uint16_t port = stoi(argv[1]);
    EnableScreen();

    Dict dic; // 定义一个字典类
    func_t translate = std::bind(&Dict::Translate, &dic, std::placeholders::_1);
    unique_ptr<UdpServer> usvr = make_unique<UdpServer>(translate, port);
    usvr->InitServer();
    usvr->Start();
    return 0;
}
#include "TcpServer.hpp"
#include <iostream>
#include <memory>
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " local-port" << std::endl;
        exit(0);
    }

    uint16_t port = std::stoi(argv[1]);
    std::unique_ptr<TcpServer> tcvr = make_unique<TcpServer>(port);
    tcvr->InitServer();
    tcvr->Loop();

    return 0;
}
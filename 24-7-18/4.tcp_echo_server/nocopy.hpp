#pragma once

// 设计一个禁止拷贝和赋值的类，其派生类也不允许拷贝和赋值
class nocopy
{
public:
    nocopy(){};
    ~nocopy(){};
    nocopy(const nocopy &) = delete;
    const nocopy &operator=(const nocopy &) = delete;
};
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <iostream>
#include <string>

using namespace std;

int main()
{
    // 1.获取共享内存
    // 获得key
    string pathname = "/home/tsx/linux/24-5-24/4.shm";
    key_t k = ftok(pathname.c_str(), 0X66);
    int shm = shmget(k, 4096, IPC_CREAT | 0666);
    cout << "已经创建好了好了" << endl;
    // 连接共享内存
    char *shmaddr = (char *)shmat(shm, nullptr, 0);
    int cnt = 5;
    while (cnt--)
    {
        cout << "shm memory content: " << shmaddr << endl; // 读取共享内存中的数据
        sleep(1);
    }

    return 0;
}
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <iostream>
#include <string>

using namespace std;

int main()
{
    // 1.创建共享内存
    // 获得key
    string pathname = "/home/tsx/linux/24-5-24/4.shm";
    key_t k = ftok(pathname.c_str(), 0X66);
    int shm = shmget(k, 4096, IPC_CREAT | IPC_EXCL | 0666);
    cout << "已经创建好了好了" << endl;
    // 连接共享内存
    char *shmaddr = (char *)shmat(shm, nullptr, 0);
    int cnt = 5;
    while (cnt--)
    {
        sprintf(shmaddr, "i am process A ");

        sleep(1);
    }

    return 0;
}
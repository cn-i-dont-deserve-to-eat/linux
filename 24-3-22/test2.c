#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<stdlib.h>
int main(){
  pid_t id=fork();
  if(id<0){
    perror("fork");
    return 1;
  }
  if(id==0){
    //child
    printf("i am child process pid:%d and begin z...\n",getpid());
    sleep(5);
    exit(EXIT_SUCCESS);
  }else{
    printf("i am father process pid:%d and slepping\n",getpid());
    sleep(30);
  }

  return 0;
}

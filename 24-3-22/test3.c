#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>

int main(){
  pid_t id=fork();
  if(id<0){
    perror("fork");
    return 1;
  }else if(id==0){
    //child
    while(1){
      printf("i am child process pid:%d ppid:%d\n",getpid(),getppid());
      sleep(1);
    }
  }else{
    int cnt=5;
    while(cnt){
      cnt--;
      printf("i am father process pid:%d ppid:%d\n",getpid(),getppid());
      sleep(1);
    }
  }

  return 0;
}

#include "TcpServer.hpp"
#include "Http.hpp"
#include <string>
using std::cout;
using std::endl;
using std::string;
HttpRespopnse Login(HttpRequest &req)
{
    HttpRespopnse resp;
    cout << "外部拿到参数: " << endl;
    req.GetResuestBody();
    cout << "##########################" << endl;
    resp.AddCode(200, "OK");
    resp.AddBodyText("<html><h1>result done!</h1></html>");
    return resp;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " local-port" << std::endl;
        exit(0);
    }
    uint16_t port = std::stoi(argv[1]);
    HttpServer hserver;
    hserver.InsertService("/login", Login);

    std::unique_ptr<TcpServer> tsvr = std::make_unique<TcpServer>(
        std::bind(&HttpServer::HandlerHttpRequest, &hserver, std::placeholders::_1), port);
    tsvr->Loop();

    return 0;
}
#pragma once
#include <functional>
#include "Socket.hpp"
#include "Log.hpp"
#include "InetAddr.hpp"

using namespace socket_ns;
static const int gport = 8888;

using service_t = std::function<std::string(std::string &requeststr)>; // 一个回调函数，传入一个请求字符串得到一个处理过的应答字符串

class TcpServer
{
public:
    TcpServer(service_t service, int port = gport)
        : _service(service), _port(port), _listensock(std::make_shared<TcpSocket>()), _isrunning(false)
    {
        _listensock->BuildListenSocket(_port);
    }
    ~TcpServer() {}

    class ThreadData
    {
    public:
        SockSPtr _sockfd;
        TcpServer *_self;
        InetAddr _addr;

        ThreadData(SockSPtr sockfd, TcpServer *self, const InetAddr &addr)
            : _sockfd(sockfd), _self(self), _addr(addr)
        {
        }
    };

    void Loop()
    {
        _isrunning = true;
        while (_isrunning)
        {
            InetAddr client;
            SockSPtr newsock = _listensock->Accepter(&client);
            if (newsock == nullptr)
            {
                continue;
            }
            LOG(INFO, "get a link,client info : %s,sockfd is :%d\n", client.AddrStr().c_str(), newsock->Sockfd());

            pthread_t tid;
            ThreadData *td = new ThreadData(newsock, this, client);
            pthread_create(&tid, nullptr, Execute, td);
        }
        _isrunning = false;
    }

    static void *Execute(void *arg)
    {
        pthread_detach(pthread_self());
        ThreadData *td = static_cast<ThreadData *>(arg);
        std::string requeststr;
        ssize_t n = td->_sockfd->Recv(&requeststr); // 收到请求字符串
        if (n > 0)
        {
            std::string responsestr = td->_self->_service(requeststr); // 获取应答字符串
            td->_sockfd->Send(responsestr);                            // 发送应答
        }
        td->_sockfd->Close();
        delete td;
        return nullptr;
    }

private:
    uint16_t _port;
    SockSPtr _listensock;
    bool _isrunning;
    service_t _service;
};

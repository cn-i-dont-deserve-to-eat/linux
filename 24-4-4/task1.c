#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>

int main(){
  pid_t id=fork();
  if(id<0){
    perror("fork");
    return 1;
  }
  if(id==0){
    //child
    int cnt=5;
    while(cnt){
      printf("i am child process pid:%d ppid:%d\n",getpid(),getppid());
      cnt--;
      sleep(1);
    }

  }else{
    sleep(6);
    printf("i am father process pid:%d ppid:%d\n",getpid(),getppid());
     
  }


  return 0;
}

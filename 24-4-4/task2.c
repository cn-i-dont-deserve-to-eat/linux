#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int main(){
  
  pid_t pid=fork();
  if(pid<0){
    perror("fork");
    exit(1);
  }
  
  if(pid==0){
    int cnt=5;
    while(cnt){
      printf("i am child process pid:%d ppid:%d\n",getpid(),getppid());
      cnt--;
      sleep(1);
    }
  }else{
    sleep(10);
    printf("i am father process pid:%d ppid:%d\n",getpid(),getppid());
  }
  return 0;
}

#include<stdio.h>                                                          
#include<unistd.h>    
#include<stdlib.h>
#include<wait.h>
#include<sys/types.h>
int main(){    
    
  pid_t pid=fork();    
  if(pid<0){    
    perror("fork");    
    exit(1);    
  }    
    
  if(pid==0){    
   
    execlp("ls","ls","-l","-a",NULL);
    
    
    // int cnt=5;    
   // while(cnt){    
   //   printf("i am child process pid:%d ppid:%d\n",getpid(),getppid());    
   //   cnt--;    
   //   sleep(1);    
   // }    
  }
  int status=0;
  int ret=waitpid(pid,&status,0);
  if(ret>0){
    if(WIFEXITED(status)){
      printf("exit success exitcode:%d\n",WEXITSTATUS(status));
    }else{
      printf("exit fault sigcode:%d\n",WSTOPSIG(status));
    }
  }
  return 0;    
} 

#pragma once
#include <iostream>
#include <functional>
#include <string>
#include <queue>
#include <pthread.h>

using namespace std;
const static int defaultcap = 5;

template <class T>

class BlockingQueue
{
private:
    // 判断是否为空
    bool IsEmpty()
    {
        return _block_queue.empty();
    }
    // 判断是否为满
    bool IsFull()
    {
        return _block_queue.size() == _maxcap;
    }

public:
    // 构造
    BlockingQueue(int cap = defaultcap)
        : _maxcap(cap)
    {
        pthread_mutex_init(&_mutex, NULL);
        pthread_cond_init(&_p_cond, NULL);
        pthread_cond_init(&_c_cond, NULL);
    }
    // 弹出元素，即消费者取出元素
    void Pop(T *out)
    {
        pthread_mutex_lock(&_mutex);
        while (IsEmpty())
        { // 队列为空没有元素，线程进入等待
            pthread_cond_wait(&_c_cond, &_mutex);
        }
        // 此时队列一定有元素
        *out = _block_queue.front();
        _block_queue.pop();
        pthread_cond_signal(&_p_cond); // 唤醒一个生产者
        pthread_mutex_unlock(&_mutex);
    }

    // 入队列，生产者生产元素
    void Push(const T &val)
    {
        pthread_mutex_lock(&_mutex);
        while (IsFull())
        { // 队列为满，生产者线程进入等待
            pthread_cond_wait(&_p_cond, &_mutex);
        }
        // 此时队列一定不为满
        _block_queue.push(val);
        pthread_cond_signal(&_c_cond); // 唤醒一个消费者
        pthread_mutex_unlock(&_mutex);
    }

    // 析构
    ~BlockingQueue()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_p_cond);
        pthread_cond_destroy(&_c_cond);
    }

private:
    queue<T> _block_queue;  // 底层容器存储临界资源
    int _maxcap;            // 队列最大容量
    pthread_mutex_t _mutex; // 互斥锁，实现同步
    pthread_cond_t _p_cond; // 生产者条件变量
    pthread_cond_t _c_cond; // 消费者条件变量
};
#include <iostream>
#include "blockingqueue.hpp"
#include <unistd.h>
#include <ctime>

using namespace std;

void *Consumer(void *arg)
{
    BlockingQueue<int> *bq = static_cast<BlockingQueue<int> *>(arg); // 类型转换
    while (true)
    {
        int val = 0;
        bq->Pop(&val);
        cout << "消费者取出数据->" << val << endl;
        sleep(1);
    }
}

void *Produce(void *arg)
{
    BlockingQueue<int> *bq = static_cast<BlockingQueue<int> *>(arg); // 类型转换
    while (true)
    {
        int val = rand() % 100;
        bq->Push(val);
        cout << "生产者生产数据->" << val << endl;
        sleep(1);
    }
}

int main()
{
    BlockingQueue<int> *bq = new BlockingQueue<int>();
    srand(time(nullptr));
    pthread_t t1, t2, t3, t4;
    pthread_create(&t1, NULL, Consumer, bq);
    pthread_create(&t2, NULL, Produce, bq);
    pthread_create(&t3, NULL, Consumer, bq);
    pthread_create(&t4, NULL, Produce, bq);

    pthread_join(t1, nullptr);
    pthread_join(t2, nullptr);
    pthread_join(t3, nullptr);
    pthread_join(t4, nullptr);
    return 0;
}
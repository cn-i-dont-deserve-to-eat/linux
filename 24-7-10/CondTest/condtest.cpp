#include <iostream>
#include <unistd.h>
#include <pthread.h>

using namespace std;

pthread_cond_t cond;
pthread_mutex_t mutex;

void *r1(void *arg) // 等待函数，执行该函数的线程一直处于while (true)
{
    pthread_cond_wait(&cond, &mutex);
    cout << "被唤醒" << endl;

    return arg;
}

void *r2(void *arg) // 唤醒函数，执行该函数的线程一直尝试唤醒某个等待的线程
{

    while (true)
    {
        pthread_cond_signal(&cond);
        cout << "唤醒某个线程" << endl;
        sleep(1);
    }
}

int main()
{
    pthread_t t1, t2; // 定义两个线程

    pthread_cond_init(&cond, NULL);   // 初始化条件变量
    pthread_mutex_init(&mutex, NULL); // 初始化互斥锁

    pthread_create(&t1, NULL, r1, NULL); // 创建线程并分配执行函数
    pthread_create(&t1, NULL, r2, NULL);

    pthread_join(t1, NULL); // 等待线程退出
    pthread_join(t2, NULL);

    pthread_mutex_destroy(&mutex); // 销毁互斥锁和条件变量
    pthread_cond_destroy(&cond);

    return 0;
}
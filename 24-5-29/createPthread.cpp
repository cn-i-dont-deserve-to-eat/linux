#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <string.h>

using namespace std;

void *rout(void *arg)
{
    while (true)
    {
        cout << "thread id : " << pthread_self() << " i am thread num: " << *(int *)arg << endl;
        sleep(1);
    }
    return NULL;
}

int main()
{
    pthread_t tid;
    int num = 10;
    int res = pthread_create(&tid, NULL, rout, (void *)(&num));
    if (res != 0)
    {
        fprintf(stderr, "pthread_create: %s\n", strerror(res));
        exit(1);
    }
    while (true)
    {
        cout << "tid: " << tid << "  I am main thread" << endl;
        sleep(1);
    }
    return 0;
}

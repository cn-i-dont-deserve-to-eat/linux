#include <stdio.h>
#include <signal.h>
#include <unistd.h>

using namespace std;

int flag = 0;

void myhandler(int sig)
{
    printf("flag to 1\n");
    flag = 1;
}

int main()
{

    signal(2, myhandler);

    while (!flag)
    {
        printf("i am process\n");
        sleep(1);
    }
    printf("process end\n");

    return 0;
}
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <string.h>
using namespace std;

void *rout(void *arg)
{
    int cnt = 3;
    int *status = new int(20);
    while (true)
    {
        cout << "thread id :  " << pthread_self() << "  i am thread num:  " << *(int *)arg << endl;
        sleep(1);
        if (cnt == 0)
        {
            cout << "phread exit....." << endl;
            exit(0);
        }
        cnt--;
    }
    return NULL;
}

int main()
{
    pthread_t tid;
    int num = 10;
    int res = pthread_create(&tid, NULL, rout, (void *)(&num));
    void *status = NULL;

    // sleep(5);
    // pthread_cancel(tid); // 强制终止tid
    cout << "thread is end" << endl;
    pthread_join(tid, &status); // 等待线程结束
    sleep(3);
    cout << "wait success! main thread end! status: " << *(int *)(status) << endl;
    return 0;
}
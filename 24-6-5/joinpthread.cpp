#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <string.h>
using namespace std;

void *rout(void *arg)
{
    int cnt = 3;
    int *status = new int(20);
    int num = 20;
    while (cnt--)
    {
        cout << "thread id :  " << pthread_self() << "  i am thread num:  " << *(int *)arg << endl;
        sleep(1);
    }
    pthread_exit((void *)status);
    // return (void *)status;
}

int main()
{
    pthread_t tid;
    int num = 10;
    int res = pthread_create(&tid, NULL, rout, (void *)(&num));
    void *status = NULL;
    sleep(2);
    pthread_cancel(tid);
    pthread_join(tid, &status); // 等待线程结束
    if (status == PTHREAD_CANCELED)
    {
        cout << "pthread is cancel" << endl;
    }
    return 0;
}
#include "LockGuard.hpp"
#include "Log.hpp"

using namespace log_ns;

int main()
{
    // EnableFile();
    EnableFile();
    LOG(1, "test1");
    LOG(1, "test1");
    LOG(1, "test1");
    EnableScreen();
    LOG(2, "test2");
    LOG(2, "test2");
    LOG(2, "test2");
    return 0;
}
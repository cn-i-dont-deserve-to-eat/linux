#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
int main(){
  pid_t id=fork();
  //printf("fork_id:%d\n",id);
  if(id==0){
    //child
    printf("I am child process,pid:%d ppid:%d \n",getpid(),getppid());
    return 0;
  }else if(id>0){
    //father
    printf("I am father process,pid:%d ppid:%d \n",getpid(),getppid());
    return 0;
  }else{
    printf("fork fail\n");
  }


  return 0;
}

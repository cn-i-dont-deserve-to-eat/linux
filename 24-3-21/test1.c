#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>

int val=0;

int main(){
  
  pid_t id=fork();
  if(id==0){
    //child
    val+=2;
    printf("i am child,pid:%d ppid:%d val:%d &val:%p\n",getpid(),getppid(),val,&val);
  }else{
    val++;
    printf("i am father,pid:%d ppid:%d val:%d &val:%p\n",getpid(),getppid(),val,&val);
  }

  return 0;
}

#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
using namespace std;

void myhandler(int sig)
{
    cout << "进程" << getpid() << " 接收到了sig信号: " << sig << endl;
}

int main()
{
    signal(2, myhandler);
    while (true)
    {
        cout << "hello" << endl;
        sleep(1);
    }

    return 0;
}
#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<wait.h>
int main(){
  pid_t id=fork();
  if(id==0){
    int cnt=5;
    while(cnt){
      printf("i am child process pid:%d ppid:%d\n",getpid(),getppid());
      sleep(1);
      cnt--;
    }
  }
  printf("wait begin\n");
  while(1){
    pid_t wid= waitpid(id,0,WNOHANG);
    if(wid==0){
       printf("i am waiting\n");
       sleep(1);
    }else{
      break;
    }
  }
  
  printf("wait success\n");
  return 0;
}
//int main(){
//  pid_t id=fork();
//  if(id==0){
//    int cnt=5;
//    while(cnt){
//      printf("i am child process pid:%d ppid:%d\n",getpid(),getppid());
//      sleep(1);
//    }
//  }
//
//  waitpid(id,0,0);
//  printf("wait success\n");
//  return 0;
//}

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <fcntl.h>

using namespace std;

int main()
{
    int fd[2];
    int res = pipe(fd);
    if (res < 0)
    {
        perror("pipe");
    }

    pid_t id = fork();
    if (id == 0)
    {
        close(fd[1]);

        while (true)
        {
            char buff[1024];
            ssize_t sz = read(fd[0], buff, sizeof buff);
            if (sz == 0)
            {
                exit(1);
            }
            else if (sz < 0)
            {
                perror("read");
            }
            else
            {
                buff[sz] = 0;
                cout << buff << endl;
            }
            sleep(1);
        }
    }
    close(fd[0]);
    while (true)
    {
        string message = "i am father";
        int n = write(fd[1], message.c_str(), message.size());
        sleep(1);
    }
    return 0;
}

#include <iostream>
#include <ctime>
#include <sys/stat.h>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#define TASKNUM 3
using namespace std;

typedef void (*task_t)(); // task_t函数指针类型

void Print()
{ // 任务1
    cout << "Printf task" << endl;
}

void DownLoad()
{ // 任务2
    cout << "DownLoad task" << endl;
}

void Flush()
{ // 任务3
    cout << "Flush task" << endl;
}

task_t tasks[TASKNUM]; // 用来存放函数指针

void LoadTask()
{
    srand(time(nullptr));
    tasks[0] = Print;
    tasks[1] = DownLoad;
    tasks[2] = Flush;
}

void ExcuteTask(int n)
{
    if (n < 0 || n > 2)
        return;

    tasks[n]();
}

int SelectTask()
{
    int n = rand() % TASKNUM;
    return n;
}

void work1()
{
    while (true)
    {
        int command = 0;
        int n = read(0, &command, sizeof command); // 读取管道中的任务编号(int)
        if (n == sizeof(int))
        {
            ExcuteTask(command);
        }
        else if (n == 0)
        {
            cout << "sub process: " << getpid() << " quit " << endl;
            break;
        }
    }
}
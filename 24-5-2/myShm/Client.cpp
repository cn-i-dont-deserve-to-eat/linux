#include "shm.hpp"
#include "NamePipe.hpp"
#include <iostream>

using namespace std;

int main()
{
    // 创建共享内存
    Shm shm(pathname, proj_id, gUser);
    // shm.InitShmaddr();
    char *shmaddr = (char *)shm.GetShmaddr();

    sleep(3);
    // 创建命名管道
    NamePipe fifo(comm_path, User);
    fifo.OpenForWrite();

    char ch = 'A';
    while (ch <= 'Z')
    {
        shmaddr[ch - 'A'] = ch;
        string message = "weakup";
        fifo.WriteNamePipe(message);
        cout << "add: " << ch << " to Server" << endl;
        sleep(2);
        ch++;
    }
    return 0;
}
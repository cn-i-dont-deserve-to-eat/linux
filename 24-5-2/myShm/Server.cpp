#include "shm.hpp"
#include "NamePipe.hpp"
#include <iostream>

using namespace std;

int main()
{
    // 创建共享内存x
    Shm shm(pathname, proj_id, gCreater);
    char *shmaddr = (char *)shm.GetShmaddr();
    shm.InitShmaddr();
    sleep(3);
    // 创建命名管道
    NamePipe fifo(comm_path, Creater);
    fifo.OpenForRead();

    while (true)
    {
        string message = "";
        fifo.ReadNamePipe(message);
        cout << "shm memory content: " << shmaddr << endl; // 读取共享内存中的数据
    }
    return 0;
}
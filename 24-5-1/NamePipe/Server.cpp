#include "NamePipe.hpp"

// Read
int main()
{
    NamePipe fifo(comm_path, Creater);
    // 如果写端没有打开，就会阻塞等待
    cout << "Server 在等待Client打开文件....." << endl;
    sleep(2);
    cout << fifo.OpenForRead() << endl;
    if (fifo.OpenForRead())
    {
        cout << "server 已经打开管道，准备通信" << endl;
        while (true)
        {
            string message = "";
            int n = fifo.ReadNamePipe(message);
            // cout<<message<<endl;
            if (n > 0)
            {
                // 读取成功
                cout << message << endl;
            }
            else if (n == 0)
            {
                // 写端已经关闭，直接退出
                break;
            }
            else
            {
                // 读取失败
                cout << "fifo.ReadNamePipe error" << endl;
                break;
            }
        }
    }
    return 0;
}
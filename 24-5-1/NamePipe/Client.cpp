#include "NamePipe.hpp"

// Write
int main()
{
    NamePipe fifo(comm_path, User);
    // 如果读端没有打开，就会阻塞等待
    cout << "Client 在等待Server打开文件....." << endl;
    if (fifo.OpenForWrite())
    {
        cout << "client 已经打开管道，准备通信" << endl;
        while (true)
        {
            string message = "";
            cout << "Please Input: ";
            getline(cin, message);
            fifo.WriteNamePipe(message);
        }
    }
    return 0;
}
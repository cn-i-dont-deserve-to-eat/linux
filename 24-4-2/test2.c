#include<stdio.h>
#include<sys/types.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<unistd.h>
#include<string.h>
char* filename="log.txt";

int main(){
  int fd1=open(filename,O_WRONLY|O_CREAT|O_TRUNC,0666);
  printf("fd1:%d\n",fd1);
  int fd2=dup(fd1);
  printf("fd2:%d\n",fd2);
  char* mage1="mage1 hello linux\n";
  char* mage2="mage2 hello linux\n";
  write(fd1,mage1,strlen(mage1));
  close(fd1);
  write(fd2,mage2,strlen(mage2));
  close(fd2);
  return 0;
}

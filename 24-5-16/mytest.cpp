#include <iostream>
#include <signal.h>

using namespace std;

void PrintSet(sigset_t *set)
{
    for (int i = 31; i >= 1; i--) // 1-31号信号
    {
        if (sigismember(set, i))
        {
            putchar('1');
        }
        else
        {
            putchar('0');
        }
    }
    puts("");
}

int main()
{
    sigset_t s, p;
    sigemptyset(&s);
    sigaddset(&s, SIGINT);
    sigprocmask(SIG_BLOCK, &s, NULL);
    while (1)
    {
        sigpending(&s);
        PrintSet(&s);
        sleep(1);
    }

    return 0;
}
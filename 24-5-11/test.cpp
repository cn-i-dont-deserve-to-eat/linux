
#include <iostream>
#include <algorithm>
using namespace std;
class Base1
{
public:
    virtual void func1() { cout << "Base1::func1" << endl; }
    virtual void func2() { cout << "Base1::func2" << endl; }
    int _b;
};

class Base2
{
public:
    virtual void func1() { cout << "Base2::func1" << endl; }
    virtual void func2() { cout << "Base2::func2" << endl; }
    int _c;
};

class Derive : public Base1, public Base2
{
public:
    virtual void func1() { cout << "Derive::func1" << endl; }
    virtual void func3() { cout << "derive::func3" << endl; }
    int _d;
};
int main()
{
    Derive d;
    Base1 b;
    cout << sizeof(d) << endl;
    cout << sizeof(b) << endl;
    d._b = 2;
    d._c = 3;
    d._d = 4;

    return 0;
}
// 这个是代码